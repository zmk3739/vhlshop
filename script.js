var currPage = "home";
var navPage = [];
var mobileNavbarOpen = false;

function navigate(page) {
    //push onto the navigation stack and change current page
    if(page == 'home') {
        navPage = [];
    } else {
        navPage.push(currPage);
    }
    currPage = page;

    contentChange(page);
}

function navBack() {
    currPage = navPage.pop();

    contentChange(currPage);
}

function filter(input) {
    var targets = document.getElementsByClassName(input.id);
    var hideShow = '';

    if(input.checked) {
        hideShow = 'inline-block';
    } else {
        hideShow = 'none';
    }

    for(let i = 0; i < targets.length; i++) {
        targets[i].style.display = hideShow;
    }
}

function contentChange(page) {
    var contentBox = document.getElementById("content");
    contentBox.classList.add("fadeOut");

    setTimeout(function() {
        swapContent(page);
        setTimeout(function() {
            contentBox.classList.remove("fadeOut");
            contentBox.classList.add("fadeIn");
        }, 200);
    }, 700);

    contentBox.classList.remove("fadeIn");
}

function swapContent(page) {
    var contentSource = "pages/" + page + ".html";
    var contentDiv = document.getElementById('content');

    //send the request, put file content into content div
    var xhr = new XMLHttpRequest();
    xhr.open('GET', contentSource, true);
    xhr.onreadystatechange = function() {
        if (this.readyState !== 4) return;
        if (this.status !== 200) return;
        contentDiv.innerHTML = this.responseText;
    };
    xhr.send();
}

//----------------------------------mobile exclusive stuff

/*
either expands or collapses the navbar, depending on whether or not the
button has been clicked already. if the button was previously clicked, collapse.
if it wasnt, expand. swap the image displayed on the button as necessary
*/
function navbarExpand() {
    var navbar = document.getElementById('mobile-navbar-expanded');

    if (mobileNavbarOpen == false) { //if the navbar isn't open
        navbar.style.display = 'block';
    } else { //the navbar is open
        navbar.style.display = 'none';
    }

    swapButtonImage();
}

/*
swaps the button image from hamburger to X, and vice versa
*/
function swapButtonImage() {
    var buttonImage = document.getElementById('mobile-button');

    if (mobileNavbarOpen) { //button has been clicked before
        buttonImage.src = 'img/hamburger.gif';
        mobileNavbarOpen = false;
    } else { //button has not been clicked before
        buttonImage.src = 'img/x.gif';
        mobileNavbarOpen = true;
    }
}

/*
change the content of the page depending on which button we clicked. make sure
to pass a pageID when calling the function from onclick. when we navigate, hide
the expanded navbar and change the button image back to its default value
*/
function navigateMobile(page) {
    document.getElementById('mobile-navbar-expanded').style.display = 'none';
    navigate(page);
    swapButtonImage();
}